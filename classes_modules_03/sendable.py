#! /usr/bin/env python3

import time

class Sendable:
    _history = {}

    def __init__(self, *, _body = None, _subject = None, _from = None, _to = None):
        self._body = _body
        self._subject = _subject
        self._from = _from
        self._to = _to
        self._created_at = time.time()
        self._update_at = time.time()
        self._sent_at = None

    def send(self):
        if self._sent_at is None:
            self._sent_at = time.time()
            if self._from not in Sendable._history:
                Sendable._history[self._from] = {self._to : []}
            if self._to not in Sendable._history[self._from]:
                Sendable._history[self._from][self._to] = [self._sent_at]
            else:
                Sendable._history[self._from][self._to].append(self._sent_at)
        else:
            raise RuntimeError("DataAlreadySent")

