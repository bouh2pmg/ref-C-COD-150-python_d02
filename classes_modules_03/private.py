#! /usr/bin/env python3

from sendable import Sendable

class Private(Sendable):

    def __init__(self, *, _body = None, _subject = None, _from = None, _to = None):
        super().__init__(_body = _body, _subject = _subject, _from = _from, _to = _to)
        self._sent = False

    def send(self):
        if not self._sent:
            self._sent = True
        else:
            raise RuntimeError("DataAlreadySent")
