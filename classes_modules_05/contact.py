#! /usr/bin/env python3

class Addressable:
    def __init__(self, _address = ""):
        self._address = _address

class HasFriends:
    def __init__(self, _friends = []):
        self._friends = _friends

    def friends(self):
       return self._friends

class Nameable:
    def __init__(self, _name = ""):
        self._name = _name

class Contact(Addressable, Nameable, HasFriends):
    def __init__(self, *, _address = "", _name = "", _friends = []):
        Addressable.__init__(self, _address)
        Nameable.__init__(self, _name)
        HasFriends.__init__(self, _friends)

