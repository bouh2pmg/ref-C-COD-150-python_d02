#! /usr/bin/env python3

import threading
import time

#from sendable import Sendable

class SendableBox:
    _all = {}

    @staticmethod
    def recv(sendable):
        recipient_box = SendableBox._all[sendable._to]
        recipient_box.__recv(sendable)

    def __box_thread(self):
        while self.max_box > len(self.read):
            time.sleep(self.sleep_duration)
            if self.box:
                self.read.append(self.box.pop())
                print("New message from " + self.read[-1]._from)
                if not self.read[-1]._subject:
                    print(self.read[-1]._body)
                else:
                    print(self.read[-1]._subject)

    def __init__(self, *, address, max_box, sleep_duration):
        self.address = address
        self.max_box = max_box
        self.sleep_duration = sleep_duration
        self.box = []
        self.read = []
        SendableBox._all[self.address] = self
        t = threading.Thread(target=self.__box_thread)
        t.start()

    def __recv(self, sendable):
        self.box.append(sendable)
