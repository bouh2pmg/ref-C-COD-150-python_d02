# /usr/bin/env python3


from sendable import Sendable

class Sms(Sendable):

    def __init__(self, *, _body = None, _from = None, _to = None):
        super().__init__(_body = _body, _subject = None, _from = _from, _to = _to)

