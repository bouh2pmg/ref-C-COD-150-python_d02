#! /usr/bin/env python3

import time

class Sendable:
    _body = None
    _subject = None
    _from = None
    _to = None
    _created_at = time.time()
    _update_at = time.time()
    _sent_at = None

    def __init__(self, *, _body = None, _subject = None, _from = None, _to = None):
        self._body = _body
        self._subject = _subject
        self._from = _from
        self._to = _to

    def send(self):
        if self._sent_at is None:
            self._sent_at = time.time()
        else:
            raise RuntimeError("DataAlreadySent")

