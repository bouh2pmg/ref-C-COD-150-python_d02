#! /usr/bin/env python3

from  sendable import Sendable

class Email(Sendable):

    def __init__(self, *, _body = None, _subject = None, _from = None, _to = None):
        super().__init__(_body = _body, _subject = _subject, _from = _from, _to = _to)

