#! /usr/bin/env python3

import time


def display_time(_time):
    print(time.strftime("%T, %A, %u* day of the %W* week of %Y, %B", time.gmtime(_time)))

